import {applyMiddleware, combineReducers, compose, createStore} from 'redux';
import thunk from 'redux-thunk';

import {showReducer as show} from "./reducers/show-reducer";
import {episodesReducer as episodes} from "./reducers/episodes-reducer";

const appReducers = combineReducers({
    show,
    episodes
});

const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

const rawStoreEnhancer = applyMiddleware(thunk);
const storeEnhancer = devToolsExtension ? compose(rawStoreEnhancer, devToolsExtension) : compose(rawStoreEnhancer);

export const PowerPuffStore = createStore(appReducers, {}, storeEnhancer);
