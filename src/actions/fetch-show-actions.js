import {ShowApiService} from "../services/show-api-service/ShowApiService";

export const FETCH_SHOW_PENDING = 'FETCH_SHOW_PENDING';
export const FETCH_SHOW_SUCCESS = 'FETCH_SHOW_SUCCESS';
export const FETCH_SHOW_ERROR = 'FETCH_SHOW_ERROR';

export const FETCH_SHOW_EPISODES_PENDING = 'FETCH_SHOW_EPISODES_PENDING';
export const FETCH_SHOW_EPISODES_SUCCESS = 'FETCH_SHOW_EPISODES_SUCCESS';
export const FETCH_SHOW_EPISODES_ERROR = 'FETCH_SHOW_EPISODES_ERROR';

function fetchShowPending() {
    return {
        type: FETCH_SHOW_PENDING
    }
}

function fetchShowSuccess(showData) {
    return {
        type: FETCH_SHOW_SUCCESS,
        showData
    }
}

function fetchShowError(error) {
    return {
        type: FETCH_SHOW_ERROR,
        error: error
    }
}

function fetchShowEpisodesPending() {
    return {
        type: FETCH_SHOW_EPISODES_PENDING
    }
}

function fetchShowEpisodesSuccess(episodesData) {
    return {
        type: FETCH_SHOW_EPISODES_SUCCESS,
        episodesData
    }
}

function fetchShowEpisodesError(error) {
    return {
        type: FETCH_SHOW_EPISODES_ERROR,
        error: error
    }
}

export function fetchShowEpisodesData(showId) {
    return async dispatch => {
        // TODO: prevent same data from being fetched twice if not necessary
        dispatch(fetchShowEpisodesPending());
        try {
            const rawResult = await ShowApiService.fetchShowEpisodes(showId);
            const parsedResult = await rawResult.json();
            if(parsedResult.error) {
                dispatch(fetchShowEpisodesError(parsedResult.error));
                return;
            }
            dispatch(fetchShowEpisodesSuccess(parsedResult));
        } catch (e) {
            dispatch(fetchShowEpisodesError(e));
        }

    }
}

export function fetchShowData(showId) {
    return async dispatch => {
        // TODO: prevent same data from being fetched twice if not necessary
        dispatch(fetchShowPending());
        try {
            const rawResult = await ShowApiService.fetchShowData(showId);
            const parsedResult = await rawResult.json();
            if(parsedResult.error) {
                dispatch(fetchShowError(parsedResult.error));
                return;
            }
            dispatch(fetchShowSuccess(parsedResult));
        } catch (e) {
            dispatch(fetchShowError(e));
        }

    }
}
