import React from 'react';
import './App.scss';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import {AppHeader} from "./components/app-header/AppHeader";
import {ShowView} from "./containers/show-view/ShowView";
import {DetailView} from "./containers/detail-view/DetailView";

function App() {
  return (
    <Router>
        <AppHeader />
        <main className={"pp-app"} id="shows-app">
          <Switch>
            <Route exact path="/" children={<ShowView />} />
            <Route path="/:movieId" children={<DetailView />} />
          </Switch>
        </main>
    </Router>
  );
}

export default App;
