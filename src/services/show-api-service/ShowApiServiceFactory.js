export function ShowApiServiceFactory(fetch) {

    /**
     * Fetch show data
     * @param showId
     * @returns {Promise<*>}
     */
    async function fetchShowData(showId) {
        return await fetch(`https://api.tvmaze.com/shows/${showId}`);
    }

    /**
     * Fetch episodes data
     * @param showId
     * @returns {Promise<*>}
     */
    async function fetchShowEpisodes(showId) {
        return await fetch(`https://api.tvmaze.com/shows/${showId}/episodes`);
    }

    return {
        fetchShowData,
        fetchShowEpisodes
    }

}
