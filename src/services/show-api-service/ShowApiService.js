import 'whatwg-fetch'
import {ShowApiServiceFactory} from "./ShowApiServiceFactory";

export const ShowApiService = new ShowApiServiceFactory(fetch);
