import {ShowApiServiceFactory} from "./ShowApiServiceFactory";

describe('ShowApiService', () => {
    it('should fetch a single show\'s data', async () => {
        const fetch = jest.fn();
        const showApiService = new ShowApiServiceFactory(fetch);
        const mockId = '123';
        const result = await showApiService.fetchShowData(mockId);
        expect(fetch).toHaveBeenCalledWith("https://api.tvmaze.com/shows/123");
    });
    it('should fetch a single show\'s episode list', async () => {
        const fetch = jest.fn();
        const showApiService = new ShowApiServiceFactory(fetch);
        const mockId = '123';
        const result = await showApiService.fetchShowEpisodes(mockId);
        expect(fetch).toHaveBeenCalledWith("https://api.tvmaze.com/shows/123/episodes");
    })
});
