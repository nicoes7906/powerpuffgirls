import {POWERPUFFGIRLS_SHOW_ID} from "../../constants/ShowsConstants";
import {fetchShowData, fetchShowEpisodesData} from "../../actions/fetch-show-actions";

/**
 * Initialization calls for the app
 * @param store
 */
export function bootstrapApp(store){
    store.dispatch(fetchShowData(POWERPUFFGIRLS_SHOW_ID));
    store.dispatch(fetchShowEpisodesData(POWERPUFFGIRLS_SHOW_ID));
}
