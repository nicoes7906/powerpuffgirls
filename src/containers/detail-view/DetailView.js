import React from 'react';
import {
    Link,
    useParams
} from "react-router-dom";
import {getEpisodesData, getEpisodesDataError, getEpisodesDataPending} from "../../reducers/episodes-reducer";
import {useSelector} from "react-redux";
import {LoadingFrame} from "../../components/loading-frame/LoadingFrame";

import "./detail-view.scss";

export function DetailView() {
    const episodeDataError = useSelector(getEpisodesDataError);
    const episodeData = useSelector(getEpisodesData);
    const episodeDataPending = useSelector(getEpisodesDataPending);

    const hasData = !episodeDataError && !episodeDataPending && episodeData && episodeData.length > 0;

    const { movieId } = useParams();

    const episodeItemData = hasData ? episodeData.find(({id}) => "" + id === movieId) : null;
    const notFoundError = !episodeItemData;

    return <>
        <header className={"detail-view__header"}>
            <div className={"container"}
            ><Link to={"/"}>Naar overzicht</Link>
            </div>
        </header>
        <LoadingFrame isLoadingError={(episodeDataError || notFoundError)} isLoading={episodeDataPending}>
            {episodeItemData && <article className={"container"}>
                <figure className={"detail-view__img-wrapper"}>
                    <img className={"detail-view__img"} alt={episodeItemData.name} src={episodeItemData.image.original} />
                </figure>
                <h1>{episodeItemData.name}</h1>
                <span className={"detail-view__meta"}>{episodeItemData.season} | {episodeItemData.number} | {episodeItemData.airdate}</span>
                <div className={"detail-view__summary"} dangerouslySetInnerHTML={{__html: episodeItemData.summary}} />
            </article>}
        </LoadingFrame>
    </>
}
