import React from 'react';
import {useSelector} from "react-redux";
import {getShowData, getShowDataError, getShowDataPending} from "../../reducers/show-reducer";

import {LoadingFrame} from "../../components/loading-frame/LoadingFrame";
import "./show-view.scss";
import {getEpisodesData, getEpisodesDataError, getEpisodesDataPending} from "../../reducers/episodes-reducer";
import {EpisodeListItem} from "../../components/episode-listitem/EpisodeListItem";

export function ShowView() {
    const showDataError = useSelector(getShowDataError);
    const showData = useSelector(getShowData);
    const showDataPending = useSelector(getShowDataPending);
    const episodeDataError = useSelector(getEpisodesDataError);
    const episodeData = useSelector(getEpisodesData);
    const episodeDataPending = useSelector(getEpisodesDataPending);

    const {name, summary, image} = showData;

    return <>
        <LoadingFrame isLoadingError={showDataError || episodeDataError} isLoading={showDataPending || episodeDataPending}>
            <div className={"container"}>
                <section className={"show-view__top-row"}>
                    <aside className={"show-view__top-col show-view__img-wrapper"}>
                        <figure>
                            <img className={"show-view__img"} src={image && image.original} alt={name} />
                        </figure>
                    </aside>
                    <article className={"show-view__top-col show-view__top-info"}>
                        <h1>
                            {name}
                        </h1>
                        <p id={"show-view-summary"} dangerouslySetInnerHTML={{__html: summary}} />
                    </article>
                </section>
            </div>
            <div className={"container"}>
                <section className={"show-view__episodes-row"}>
                    <h2>Episodes</h2>
                    <ul className={"show-view__episodes-list"}>
                        {episodeData.map(itemData => <EpisodeListItem key={itemData.id} episodeItemData={itemData}/>)}
                    </ul>
                </section>
            </div>
        </LoadingFrame>
    </>
}
