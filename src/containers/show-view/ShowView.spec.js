import {shallow} from "enzyme";
import React from "react";
import {ShowView} from "./ShowView";
import * as ReactRedux from "react-redux";

describe('ShowView', () => {

    it('should render show data properly', () => {
        const mockShowData = getMockShowData();

        const mockState = {
            show: {
                showData: getMockShowData(),
                pending: false,
                error: false
            },
            episodes: {
                episodesData: getMockEpisodeeData(),
                pending: false,
                error: false
            }
        };

        ReactRedux.useSelector = jest.fn().mockImplementation(selector => selector(mockState));

        const wrapper = shallow(<ShowView/>);

        expect(wrapper.contains("The Powerpuff Girls")).toEqual(true);
        expect(wrapper.exists("#show-view-summary")).toEqual(true);
        expect(wrapper.find("img").prop("src")).toEqual(mockShowData.image.original);

    });

});

function getMockEpisodeeData() {
    return [
        {
            "id": 657308,
            "name": "Escape from Monster Island",
            "season": 1,
            "number": 1,
            "airdate": "2016-04-04",
            "image": {
                "medium": "http://static.tvmaze.com/uploads/images/medium_landscape/53/132617.jpg",
                "original": "http://static.tvmaze.com/uploads/images/original_untouched/53/132617.jpg"
            },
        },
        {
            "id": 657309,
            "name": "Princess Buttercup",
            "season": 1,
            "number": 2,
            "airdate": "2016-04-04",
            "image": {
                "medium": "http://static.tvmaze.com/uploads/images/medium_landscape/53/132618.jpg",
                "original": "http://static.tvmaze.com/uploads/images/original_untouched/53/132618.jpg"
            },

        }
    ]
}

function getMockShowData() {
    return {
        "id": 6771,
        "name": "The Powerpuff Girls",
        "summary": "<p>The city of Townsville may be a beautiful, bustling metropolis, but don't be fooled! There's evil afoot! And only three things can keep the bad guys at bay: Blossom, Bubbles and Buttercup, three super-powered little girls, known to their fans (and villains everywhere) as <b>The Powerpuff Girls</b>. Juggling school, bedtimes, and beating up giant monsters may be daunting, but together the Powerpuff Girls are up to the task. Battling a who's who of evil, they show what it really means to \"fight like a girl.\"</p>",
        "image": {
            "medium": "http://static.tvmaze.com/uploads/images/medium_portrait/60/151357.jpg",
            "original": "http://static.tvmaze.com/uploads/images/original_untouched/60/151357.jpg"
        },
    }
}
