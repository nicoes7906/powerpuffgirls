import React from 'react';
import './loading-frame.scss';

export function LoadingFrame({children, isLoading, isLoadingError}) {
    const showFrame = isLoading || isLoadingError;
    const showError = isLoadingError;
    const showSpinner = !isLoadingError && isLoading;
    const showChildren = !isLoadingError && !isLoading;
    return <>
        {
            showFrame && <div className={"loading-frame"}>
                <div className={"loading-frame__overlay"}>
                    { showSpinner && <div id="loading-frame-spinner" className={"loading-frame__spinner"}>Loading...</div> }
                    { showError && <div id="loading-frame-error" className={"loading-frame__error"}>Uh, oh. Something went wrong.</div> }
                </div>
            </div>
        }
        {showChildren && children}
    </>;
}
