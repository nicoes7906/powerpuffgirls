import {shallow} from "enzyme";
import React from "react";
import {LoadingFrame} from "./LoadingFrame";

describe('LoadingFrame', () => {

    it('should display an error if an error has occurred', () => {
        const wrapper = shallow(<LoadingFrame isLoading={false} isLoadingError={true}><span id="children">children</span></LoadingFrame>);
        expect(wrapper.exists("#loading-frame-error")).toEqual(true);
        expect(wrapper.exists("#loading-frame-spinner")).toEqual(false);
        expect(wrapper.exists("#children")).toEqual(false);
    });

    it('should display a loading spinner if data is loading', () => {
        const wrapper = shallow(<LoadingFrame isLoading={true} isLoadingError={false}><span id="children">children</span></LoadingFrame>);
        expect(wrapper.exists("#loading-frame-error")).toEqual(false);
        expect(wrapper.exists("#loading-frame-spinner")).toEqual(true);
        expect(wrapper.exists("#children")).toEqual(false);
    });

    it('should render the children when it\'s not loading nor has an error', () => {
        const wrapper = shallow(<LoadingFrame isLoading={false} isLoadingError={false}><span id="children">children</span></LoadingFrame>);
        expect(wrapper.exists("#loading-frame-error")).toEqual(false);
        expect(wrapper.exists("#loading-frame-spinner")).toEqual(false);
        expect(wrapper.exists("#children")).toEqual(true);
    })


});
