import React from "react";

import "./episode-list-item.scss";
import {Link} from "react-router-dom";

export function EpisodeListItem({episodeItemData}) {
    const {name, id, season, number, airdate} = episodeItemData;
    return <li className={"episode-list-item"}>
        <Link className={"episode-list-item__title"} to={`/${id}`}>{name}</Link>
        <span className={"episode-list-item__meta"}>{season} | {number} | {airdate}</span>
    </li>
}
