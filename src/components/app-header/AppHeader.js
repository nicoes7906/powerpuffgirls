import logo from "../../logo.svg";
import React from "react";

import "./app-header.scss";

export function AppHeader() {
    return <header className="app-header">
        <img src={logo} alt={"React Logo"} width={212} height={150}/>
    </header>
}
