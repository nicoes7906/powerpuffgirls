import {FETCH_SHOW_EPISODES_ERROR, FETCH_SHOW_EPISODES_SUCCESS, FETCH_SHOW_EPISODES_PENDING} from '../actions/fetch-show-actions';

const initialState = {
    pending: true,
    episodesData: [],
    error: null
};

export function episodesReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_SHOW_EPISODES_PENDING:
            return {
                ...state,
                pending: true,
                error: null
            };
        case FETCH_SHOW_EPISODES_SUCCESS:
            return {
                ...state,
                pending: false,
                error: null,
                episodesData: action.episodesData
            };
        case FETCH_SHOW_EPISODES_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error,
                showId: null
            };
        default:
            return state;
    }
}

// Getters on state data
export const getEpisodesData = state => state.episodes.episodesData;
export const getEpisodesDataPending = state => state.episodes.pending;
export const getEpisodesDataError = state => state.episodes.error;
