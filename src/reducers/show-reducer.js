import {FETCH_SHOW_ERROR, FETCH_SHOW_PENDING, FETCH_SHOW_SUCCESS} from '../actions/fetch-show-actions';

const initialState = {
    pending: true,
    showData: {},
    error: null
};

export function showReducer(state = initialState, action) {
    switch(action.type) {
        case FETCH_SHOW_PENDING:
            return {
                ...state,
                pending: true,
                error: null
            };
        case FETCH_SHOW_SUCCESS:
            return {
                ...state,
                pending: false,
                error: null,
                showData: action.showData
            };
        case FETCH_SHOW_ERROR:
            return {
                ...state,
                pending: false,
                error: action.error
            };
        default:
            return state;
    }
}

// Getters on state data
export const getShowData = state => state.show.showData;
export const getShowDataPending = state => state.show.pending;
export const getShowDataError = state => state.show.error;
