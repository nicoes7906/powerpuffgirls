# Powerpuff Girls React assignment RTL
This repo contains the assignment for RTL by Nico Eshuis.

## Technologies
As prescribed the project was bootstrapped with Create React App.

The following technologies were added:

* Redux, data store based on Flux Architecture
* Redux Thunk, for fine-grained control over certain dispatches
* SASS, CSS precompiler
* Bootstrap, for a good design system to complete the assignment with
* Enzyme, for easy component testing with Jest
* Fetch polyfill, for ensuring cross browser compatibility with the Fetch API

## Folder structure
The following file structure has been followed:

* Actions, Redux action names and action functions
* Component, Dumb components
* Containers, Smart components = Store aware
* Reducers, Redux reducers
* Services, Services where blocks of business logic are grouped
* Style, generic styles

## Available Scripts
In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `yarn test`

Launches the test runner in the interactive watch mode.<br />

### `yarn build`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

## Testing
Unit tests were added to certain areas to demonstrate my preference for automated testing.

## Improvements
For further development the following improvements can be suggested at least:

* Remove duplication. In certain areas there is some duplication. This can be mitigated by creating utilities functions in JS and mixins in SASS.
* Prevent refetching data that is already present. I've added TODO's in the code to highlight these areas as well.
* Add Redux Promise middleware. The Thunk based functionality can also be efficiently solved with the Promise Middleware
for Pending, Success and Error states with Actions that return promises. Didn't want to add to much deps for a app this small at this point.
* Image loading. Right now the image loading moves the page. Preferably a placeholder is always there and it will give a more relaxed loading experience.
* URL structure. Right now we pass id's as a url parameter for showing the episodes. I think it would be an improvement to have a human readable value there like the episode name.
